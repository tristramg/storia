# Storia

[![pipeline status](https://framagit.org/tcit/storia/badges/master/pipeline.svg)](https://framagit.org/tcit/storia/commits/master)

[![coverage report](https://framagit.org/tcit/storia/badges/master/coverage.svg)](https://framagit.org/tcit/storia/commits/master)

Storia (temporary name) is a Storify clone written with Elixir and the Phoenix Framework for the back-end, and VueJS for the front-end.

It aims to replace Storify when the services closes.
It has the following features :

* Creating and editing "stories" from elements such as 
    * Text elements :
    * Twitter tweets ;
    * YouTube videos ;
    * or any pages with metadata information.

* Importing content from the following sources
    * Storify stories
    * Storify profiles
    * Twitter threads (tweets and replies)
    * Twitter Moments

# Installing

Everything can be found on the [wiki](https://framagit.org/tcit/storia/wikis/home)
