defmodule Storia.Services.StorifyTest do
  @moduledoc """
  Test the `Storia.Services.Import.Storify` module
  """

  use Storia.DataCase
  alias Storia.Services.Import.Storify
  alias Storia.Services.Parser.Tweet
  import Logger

  describe "importing a story" do
    test "import a story from a Storify URL" do
      story = Storify.import_from_url("https://storify.com/clarabdx/madeleineproject-saison-5")
      assert %{title: "#Madeleineproject SAISON 5"} = story
      assert %{description: "Production Lumento avec la Radio Nova"} = story
      assert %{elements: [%Tweet{text: "#Madeleineproject - Saison 5 - http://madeleineproject.fr/", force_url: "https://twitter.com/clarabdx/status/932549369840234497"}]}
    end

    @tag :pending
    test "import stories from Storify profile" do
      stories = Storify.import_from_account_name(self(), "clarabdx")
      [story | stories] = stories
      assert %{title: "#Madeleineproject SAISON 5"} = story
      [story | stories] = stories
      assert %{title: "#Madeleineproject SAISON 4"} = story
      [story | _] = stories
      assert %{title: "Les recettes de Madeleine cuisinées par les internautes"} = story
    end

    test "list stories from Storify profile" do
      stories = Storify.list_from_account_name(self(), "clarabdx")
      assert %{url: "https://storify.com/clarabdx/madeleineproject-saison-5"} = hd(stories)
    end

    test "import a tweet from a wrong URL" do
      assert {:error, :not_storify} == Storify.import_from_url("toto")
    end
  end
end
