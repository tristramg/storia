defmodule Storia.Services.TwitterMomentTest do
  @moduledoc """
  Test the `Storia.Services.Import.TwitterMoment` module
  """

  use Storia.DataCase
  alias Storia.Services.Import.TwitterMoment

  describe "importing a moment" do
    test "import a moment from html" do
      moment_html = File.read!("./test/fixtures/twitter/moment.html")
      assert {:ok, %{
        "title" => "The Obamas' wedding anniversary",
        "description" => "Yesterday, President and Mrs. Obama celebrated 23 years of marriage."
             }} = TwitterMoment.import_tweets_from_moment_html(moment_html)
    end
  end
end
