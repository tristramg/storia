defmodule Storia.Services.TwitterTweetsTest do
  @moduledoc """
  Test the `Storia.Services.Import.TwitterTweets` module
  """

  use Storia.DataCase
  alias Storia.Services.Import.TwitterTweets

  describe "importing a tweet" do
    test "import a tweet from a Twitter URL" do
      assert {:ok, %{"title" => "Tweets from nitot", "description" => "Dear Twitter, I was wondering how many https://t.co/jhfaosq9L1 users there are. Any idea?"}} = TwitterTweets.import_from_twitter_url("https://twitter.com/nitot/status/957963447513296897")
    end

    test "import a tweet from a wrong URL" do
      assert {:error, :not_tweet} == TwitterTweets.import_from_twitter_url("toto")
    end
  end
end
