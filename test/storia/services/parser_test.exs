defmodule Storia.ParserTest do
  use Storia.DataCase

  alias Storia.Services.Parser
  alias Storia.Services.Parser.{Tweet, Opengraph, Youtube, Facebook}

  @single_urls [
    %{
      url: "https://framablog.org/2018/03/21/peertube-beta-une-graine-dalternative-a-youtube-vient-declore/",
      type: "Opengraph",
      data: %Opengraph{
        title: "PeerTube bêta : une graine d’alternative à YouTube vient d’éclore – Framablog"
      }
    },
    %{
      url: "https://twitter.com/TheRedFag/status/978674197940580352",
      type: "Twitter",
      data: %Tweet{
        text: "L'emploi des mots à caractère homophobe, même sans mauvaise intention et quel que soit le contexte, contribue à banaliser l'homophobie. "
      }
    },
    %{
      url: "https://www.youtube.com/watch?v=rJGifTou5FE",
      type: "YouTube",
      data: %Youtube{
        title: "Ten Illegal Things To Do In London"
      }
    },
    %{
      url: "https://www.facebook.com/framasoft/photos/a.10151478218874615.1073741825.62346699614/10155647646844615/?type=3&theater",
      type: "Facebook",
      data: %Facebook{
        title: "Framasoft - Publications",
        description: "ToS;DR - répondre au plus grand mensonge d'Internet\nhttps://framablog.org/2018/03/27/tosdr-repondre-au-plus-grand-mensonge-dinternet/"
      }
    }
  ]

  describe "parser" do
    test "process_multiple_urls/1 with valid urls" do
      assert [
               %Tweet{
                 text: "Dear Twitter, I was wondering how many http://Storify.com users there are. Any idea?",
                 user: %{screen_name: "nitot"}
               },
              %Tweet{
                text: "Eh bien ne le répétez pas, ne le re-tweetez pas, n'en parlez pas à vos amis, ne venez pas trop nombreux, mais il semble bien que cette fois, notre serveur tient le coup... Chut ! https://beta.arretsurimages.net/ #angoisse #montéeencharge",
                user: %{screen_name: "arretsurimages"}
              }
             ] = Parser.process_multiple_urls(["https://twitter.com/nitot/status/957963447513296897", "https://twitter.com/arretsurimages/status/951521164056256513"])
    end

    # Process multiple single_urls
    Enum.each(@single_urls, fn url ->
      @url url

      test "process_single_url/1 with valid url from #{url.type}" do
        data = @url.data
        res = Parser.process_single_url(@url.url)
        assert data = res
      end
    end)
  end
end
