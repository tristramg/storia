defmodule Storia.Services.YoutubeTest do
  @moduledoc """
  Test the `Storia.Services.Parser.Youtube` module
  """

  use Storia.DataCase
  alias Storia.Services.Parser.Youtube

  describe "processing youtube videos" do

    test "processing a youtube video" do
      assert {:ok, %Youtube{
        id: "t5tBsVX5g0g",
        title: "Les gardiens du nouveau monde, doc 55', VF",
      }} = Youtube.parse_youtube_url("https://www.youtube.com/watch?v=t5tBsVX5g0g")
    end

    test "transform a list of URLs info youtube elements" do
      assert [%Youtube{}, %Youtube{}] = Youtube.parse_youtube_urls(["https://www.youtube.com/watch?v=t5tBsVX5g0g", "https://www.youtube.com/watch?v=19wToRIiYWI"])
    end
  end
end
