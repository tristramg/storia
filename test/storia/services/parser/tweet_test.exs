defmodule Storia.Services.TweetTest do
  @moduledoc """
  Test the `Storia.Services.Import.TwitterTweets` module
  """

  use Storia.DataCase
  alias Storia.Services.Parser.Tweet

  describe "processing tweets" do

   @tweet_one %{text: "Want to freak yourself out? I'm gonna show just how much of your information the likes of Facebook and Google store about you without you even realising it", id: "977559925680467968"}
   @tweet_two %{text: "Et hop, nous voici partis pour un nouveau thread ! Et cette fois-ci, nous allons parler de l'évolution de l'identité visuelle de France Télévisions. Vous êtes prêts ? Déroulez donc 😉\n\n#habillage #francetelevisions ", id: "957685409160597507"}

    test "processing a tweet from a ExTwitter tweet ID" do
      [tweet] = ExTwitter.lookup_status("977559925680467968", [tweet_mode: "extended"])
      tweet_struct = struct(Tweet, @tweet_one)
      assert tweet_struct = Tweet.process_tweet(tweet)
    end

    test "transform a list of URLs info tweets elements" do
      tweet_structs = [struct(Tweet, @tweet_one), struct(Tweet, @tweet_two)]
      assert tweet_structs = Tweet.parse_twitter_url_list(["https://twitter.com/tedromeda/status/957556639221022720", "https://twitter.com/valentinsocha/status/957685409160597507"])
    end
  end
end
