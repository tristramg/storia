defmodule Storia.Factory do
  use ExMachina.Ecto, repo: Storia.Repo

  def account_factory do
    %Storia.Accounts.Account{
      username: sequence("username"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      password_hash: "mypassword",
      role: 0,
      slug: sequence("slug"),
    }
  end

  def story_factory do
    %Storia.Stories.Story{
      title: sequence("mystory"),
      slug: sequence("my-slug"),
      description: "desc",
      elements: [],
      media: build(:file),
      account: build(:account)
    }
  end

  def file_factory do
    %Storia.Files.File{
      uuid: sequence("uuid"),
      path: sequence("path"),
      name: sequence("name"),
      account: build(:account)
    }
  end

  def import_factory do
    %Storia.Import{
      url: "https://twitter.com/nitot/status/957963447513296897",
      tentatives: 0,
      account: build(:account),
    }
  end
end
