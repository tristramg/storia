defmodule StoriaWeb.ErrorViewTest do
  use StoriaWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  @tag :pending
  test "renders 404.html" do
    assert render_to_string(StoriaWeb.ErrorView, "404.html", []) ==
           "Page not found"
  end

  @tag :pending
  test "render 500.html" do
    assert render_to_string(StoriaWeb.ErrorView, "500.html", []) ==
           "Internal server error"
  end

  @tag :pending
  test "render any other" do
    assert render_to_string(StoriaWeb.ErrorView, "505.html", []) ==
           "Internal server error"
  end
end
