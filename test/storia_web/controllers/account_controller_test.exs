defmodule StoriaWeb.AccountControllerTest do
  use StoriaWeb.ConnCase

  alias Storia.Accounts
  alias Storia.Accounts.Account
  import Storia.Factory

  @create_attrs %{email: "some@email.com", password: "some password", role: 42, username: "some username"}
  @update_attrs %{email: "some@email.updated", password: "some updated password", role: 43, username: "some updated username"}
  @invalid_attrs %{email: nil, password: nil, role: nil, username: nil}

  def fixture(:account) do
    {:ok, account} = Accounts.create_account(@create_attrs)
    account
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "create account" do
    test "renders account when data is valid", %{conn: conn} do
      conn = post conn, account_path(conn, :create), @create_attrs
      assert %{"account"=> %{"slug" => slug}} = json_response(conn, 201)

      conn = get conn, account_path(conn, :show, slug)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => slug,
        "role" => 42,
        "username" => "some username",
        "stats" => %{"stories" => 0}
      }
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, account_path(conn, :create), @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "show account" do
    setup [:create_account]

    test "show account from slug name", %{conn: conn, account: %Account{} = account} do
      conn = auth_conn(conn, account)
      account2 = insert(:account)
      conn = get conn, account_path(conn, :show, account2.slug)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => account2.slug,
        "role" => account2.role,
        "username" => account2.username,
        "stats" => %{"stories" => 0}
      }
    end

    test "show account from invalid slug name", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = get conn, account_path(conn, :show, "hey")
      assert response(conn, 404)
    end

    test "show account currently logged in", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = get conn, account_path(conn, :show_current_account)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => slug
             }
    end

    test "show account currently logged in when user is unlogged", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = get conn, account_path(conn, :show_current_account)
      assert response(conn, 401)
    end
  end

  describe "update account" do
    setup [:create_account]

    test "renders account when data is valid", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = put conn, account_path(conn, :update, slug), account: @update_attrs
      assert %{"slug" => slug} = json_response(conn, 200)["account"]

      conn = get conn, account_path(conn, :show, slug)
      account_data = json_response(conn, 200)["account"]
      assert account_data = %{
        "slug" => slug,
        "role" => 43,
        "username" => "some updated username"}
    end

    test "renders errors when data is invalid", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = put conn, account_path(conn, :update, slug), account: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete account" do
    setup [:create_account]
    import Logger

    test "deletes chosen account", %{conn: conn, account: %Account{slug: slug} = account} do
      conn = auth_conn(conn, account)
      conn = delete conn, account_path(conn, :delete, slug)
      assert response(conn, 204)

      conn = get conn, account_path(conn, :show, slug)
      assert response(conn, 404)
    end

    test "delete account not existing", %{conn: conn, account: %Account{} = account} do
      conn = auth_conn(conn, account)
      assert_error_sent 404, fn ->
        delete conn, account_path(conn, :delete, "coucou")
      end
    end

    test "delete account from another account", %{conn: conn, account: %Account{} = account} do
      conn = auth_conn(conn, account)
      account2 = insert(:account)
      conn = delete conn, account_path(conn, :delete, account2.slug)
      assert response(conn, 401)
    end
  end

  defp create_account(_) do
    account = fixture(:account)
    {:ok, account: account}
  end

  defp auth_conn(conn, %Storia.Accounts.Account{} = user) do
    {:ok, token, _claims} = StoriaWeb.Guardian.encode_and_sign(user)
    conn
    |> put_req_header("authorization", "Bearer #{token}")
    |> put_req_header("accept", "application/json")
  end
end
