defmodule StoriaWeb.StoryControllerTest do
  use StoriaWeb.ConnCase
  import Storia.Factory

  alias Storia.Stories
  alias Storia.Stories.Story

  @create_attrs %{title: "some title", description: "My description", account_id: 1, elements: []}
  @update_attrs %{title: "some updated title", description: "My updated description", elements: []}
  @invalid_attrs %{title: nil, elements: nil}

  def fixture(:story) do
    {:ok, story} = Stories.create_story(@create_attrs)
    story
  end

  setup %{conn: conn} do
    user = insert(:account)
    {:ok, conn: conn, user: user}
  end

  describe "index" do
    test "lists all stories", %{conn: conn} do
      conn = get conn, story_path(conn, :index)
      assert json_response(conn, 200)["stories"] == []
    end
  end

  describe "create story" do
    test "renders story when data is valid", %{conn: conn, user: user} do
      conn_post = auth_conn(conn, user)
      conn_post = post conn_post, story_path(conn_post, :create), story: @create_attrs
      assert %{"slug" => slug} = json_response(conn_post, 201)["story"]

      # Preview path is accessible
      conn = auth_conn(conn, user)
      conn = get conn, story_path(conn, :show_preview, slug)
      assert %{
               "title" => "some title"
             } = json_response(conn, 200)["story"]

      # Show path is inaccessible
      conn = get conn, story_path(conn, :show, slug)
      assert "Page not found" == json_response(conn, 404)["error"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = auth_conn(conn, user)
      conn = post conn, story_path(conn, :create), story: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update story" do
    setup [:create_story]

    test "renders story when data is valid", %{conn: conn, story: %Story{slug: slug} = story} do
      user = story.account
      conn_patch = auth_conn(conn, user)
      conn_patch = patch conn_patch, story_path(conn_patch, :update, slug), story: @update_attrs
      res = json_response(conn_patch, 200)
      assert %{"slug" => ^slug} = res["story"]

      # Preview path is accessible
      conn = auth_conn(conn, user)
      conn = get conn, story_path(conn, :show_preview, slug)
      assert %{
               "title" => "some updated title",
             } = json_response(conn, 200)["story"]

      # Show path is inaccessible
      conn = get conn, story_path(conn, :show, slug)
      assert json_response(conn, 404)["error"]
    end

    test "renders errors when data is invalid", %{conn: conn, story: story, user: user} do
      conn = auth_conn(conn, user)
      conn = put conn, story_path(conn, :update, story.slug), story: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "renders story public where published_at is set", %{conn: conn, story: %Story{slug: slug} = story} do
      user = story.account
      conn_patch = auth_conn(conn, user)
      now = DateTime.utc_now()
      conn_patch = patch conn_patch, story_path(conn_patch, :update, slug), story: %{published_at: now}
      res = json_response(conn_patch, 200)
      assert %{"slug" => ^slug} = res["story"]

      conn = get conn, story_path(conn, :show, slug)
      assert %{
               "published_at" => now
             } = json_response(conn, 200)["story"]
    end
  end

  describe "delete story" do
    setup [:create_story]

    test "deletes chosen story", %{conn: conn, story: story} do
      conn = auth_conn(conn, story.account)
      conn = delete conn, story_path(conn, :delete, story.slug)
      assert response(conn, 204)
      conn = get conn, story_path(conn, :show, story.slug)
      assert response(conn, 404)
    end
  end

  defp create_story(_) do
    story = insert(:story)
    {:ok, story: story}
  end

  defp auth_conn(conn, %Storia.Accounts.Account{} = user) do
    {:ok, token, _claims} = StoriaWeb.Guardian.encode_and_sign(user)
    conn
    |> put_req_header("authorization", "Bearer #{token}")
    |> put_req_header("accept", "application/json")
  end
end
