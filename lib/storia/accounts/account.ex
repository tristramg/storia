defmodule Storia.Accounts.Account do
  @moduledoc """
  Represents an user account
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Exgravatar
  import Logger
  alias Storia.Accounts.{Account, UsernameSlug}


  schema "accounts" do
    field :username, :string
    field :email, :string
    field :password_hash, :string
    field :password, :string, virtual: true
    field :role, :integer, default: 0
    field :display_name, :string
    field :avatar, :string

    field :slug, UsernameSlug.Type

    timestamps()
  end

  @doc false
  def changeset(%Account{} = account, attrs) do
    account
    |> cast(attrs, [:username, :email, :password_hash, :role, :display_name, :avatar])
    |> validate_required([:username, :email])
    |> unique_constraint(:username, [message: "registration.error.username_already_used"])
    |> unique_constraint(:email, [message: "registration.error.email_already_used"])
    |> validate_format(:email, ~r/@/)
  end

  def registration_changeset(struct, params) do
    struct
    |> changeset(params)
    |> cast(params, ~w(password)a, [])
    |> validate_required([:username, :email, :password])
    |> validate_length(:password, min: 6, max: 100, message: "registration.error.password_too_short")
    |> UsernameSlug.maybe_generate_slug()
    |> UsernameSlug.unique_constraint()
    |> gravatar()
    |> hash_password()
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true,
        changes: %{password: password}} ->
        put_change(changeset,
          :password_hash,
          Comeonin.Argon2.hashpwsalt(password))
      _ ->
        changeset
    end
  end

  defp gravatar(changeset) do
    Logger.debug(inspect changeset)
    case changeset do
      %Ecto.Changeset{valid?: true,
        changes: %{email: email}} ->
          url = gravatar_url(email, d: "404")
          case HTTPoison.get(url, [], [ssl: [{:versions, [:'tlsv1.2']}]]) do
            {:ok, response} ->
              put_change(changeset, :avatar, url)
            _ ->
              changeset
          end
      _ ->
        changeset
    end
  end
end
