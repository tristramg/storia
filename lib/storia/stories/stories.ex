defmodule Storia.Stories do
  @moduledoc """
  The Stories context.
  """

  import Ecto.Query, warn: false
  alias Storia.Services.Parser
  alias Storia.Repo

  alias Storia.Stories.{Story, Element}
  alias Storia.Accounts.Account
  import Logger

  @doc """
  Returns the list of stories.

  ## Examples

      iex> list_stories()
      [%Story{}, ...]

  """
  def list_stories(params \\ %{}) do
    query = from s in Story,
      preload: [:account, :media],
      order_by: [desc: s.inserted_at]
    Repo.paginate(query, params)
  end

  @doc """
  Returns the list of stories for an account
  """
  def list_stories_for_account(%Account{} = account, params \\ %{}) do
    query = from s in Story,
       where: s.account_id == ^account.id,
       preload: [:account, :media],
       order_by: [desc: s.inserted_at]
    Repo.paginate(query, params)
  end

  @doc """
  Returns the list of stories for an account slug
  """
  def list_stories_for_account_slug(account_slug, params \\ %{}) do
    query = from s in Story,
      join: a in Account, where: a.id == s.account_id,
      where: a.slug == ^account_slug,
      preload: [:account, :media],
      order_by: [desc: s.inserted_at]
    Repo.paginate(query, params)
  end

  def search_stories(search_term, params \\ %{}) do
    sanitized_term = like_sanitize(search_term)
    query = from s in Story,
      where: ilike(s.title, ^sanitized_term),
      preload: [:account, :media]
    Repo.paginate(query, params)
  end

  defp like_sanitize(value) do
    "%" <> String.replace(value, ~r/([\\%_])/, "\\1") <> "%"
  end

  @doc """
  Gets a single story.

  Raises `Ecto.NoResultsError` if the Story does not exist.

  ## Examples

      iex> get_story!(123)
      %Story{}

      iex> get_story!(456)
      ** (Ecto.NoResultsError)

  """
  def get_story!(id) do
    story = Repo.get!(Story, id)
    Repo.preload(story, [:account, :media])
  end

  @doc """
  Gets a single story by its slug

  Returns nil if there's no story with this slug

  ## Examples

      iex> get_story_by_slug("my story")
      %Story{}

      iex> get_story_by_slug("inexistant")
      nil
  """
  @spec get_story_by_slug(String.t, integer, integer) :: %Story{}
  def get_story_by_slug(slug, page \\ 0, page_size \\ 20, load_all \\ false) do
    Logger.debug("Calling get_story_by_slug with page #{page} and page_size #{page_size}")
    story = Repo.get_by(Story, slug: slug)
    story = Repo.preload story, [:account, :media]
    if story != nil do
      bring_elements_with_story(story, page, page_size, load_all)
    end
  end

  defp bring_elements_with_story(story, page \\ 0, page_size \\ 20, load_all \\ false) do
    nb_elements = length(story.elements)
    sliced_elements = if !load_all do Enum.slice(story.elements, (page * page_size)..(((page + 1) * page_size) - 1)) else story.elements end
    elements = Enum.map(sliced_elements, fn element -> Repo.one(from e in Element, where: e.id == ^element) end)
    Map.put(story, :elements, %{
      entries: elements,
      total_entries: nb_elements,
      total_pages: find_pages_for_elements(nb_elements, page_size),
      page_number: page,
      page_size: page_size
    })
  end

  defp find_pages_for_elements(nb_elements, page_size) do
    res = div(nb_elements, page_size)
    case rem(nb_elements, page_size) do
      0 ->
        res
      _ ->
        res + 1
    end
  end

  @doc """
  Creates a story.

  ## Examples

      iex> create_story(%{field: value})
      {:ok, %Story{}}

      iex> create_story(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_story(attrs \\ %{}) do
    story_inserted = %Story{}
      |> Story.changeset(attrs)
      |> Repo.insert()
    case story_inserted do
      {:ok, story} ->
        story = Repo.preload story, [:account, :media]
        {:ok, bring_elements_with_story(story)}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a story.

  ## Examples

      iex> update_story(story, %{field: new_value})
      {:ok, %Story{}}

      iex> update_story(story, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_story(%Story{} = story, attrs) do
    story
    |> Story.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Story.

  ## Examples

      iex> delete_story(story)
      {:ok, %Story{}}

      iex> delete_story(story)
      {:error, %Ecto.Changeset{}}

  """
  def delete_story(%Story{} = story) do
    Repo.delete(story)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking story changes.

  ## Examples

      iex> change_story(story)
      %Ecto.Changeset{source: %Story{}}

  """
  def change_story(%Story{} = story) do
    Story.changeset(story, %{})
  end

  @doc """
  Count how many stories there are for a given account
  """
  @spec count_stories_for_account(%Account{}) :: integer()
  def count_stories_for_account(%Account{} = account) do
    Repo.one(from s in Story, select: count(s.id), where: s.account_id == ^account.id)
  end

  @doc """
  Returns an `Storia.Stories.Element` by it's uuid

  ## Examples

      iex> get_element(123)
      %Element{}
  """
  @spec get_element!(integer()) :: %Element{}
  def get_element!(id) do
    Repo.get!(Element, id)
  end

  @doc """
  Returns an eventual `Storia.Stories.Element` by it's URL

  ## Examples

      iex> get_element_by_url("https://twitter.com/nitot/status/957963447513296897")
      %Element{}
  """
  @spec get_element_by_url(String.t) :: %Element{}
  def get_element_by_url(url) do
    Repo.get_by(Element, url: url)
  end

  import Logger
  @doc """
  Creates multiple elements

  ## Examples

      iex> create_elements(elements)
      [%Element{}]

  """
  @spec create_elements(list(struct())) :: list(%Element{})
  def create_elements(elements) do
    Enum.map(elements, fn element ->
      case url_from_element(element) do
        nil ->
          Logger.debug("Element doesn't have a force_url : so it's text")
          {:ok, produced_element} = create_element(element)
          produced_element
        url ->
          return_element(element, url)
      end
    end)
  end

  defp return_element(element, url) do
    Logger.debug("Element does have a force_url")
    case get_element_by_url(url) do
      nil ->
        Logger.debug("Created new element")
        {:ok, produced_element} = create_element(element)
        produced_element
      produced_element ->
        Logger.debug("Found existing element")
        produced_element
    end

  end

  @doc """
  Creates an element

  ## Examples

      iex> create_element(element)
      %Element{}

  """
  @spec create_element(struct()) :: %Element{}
  def create_element(element) do
    %Element{}
     |> Element.changeset(%{"data" => maps_with_string_keys(Map.from_struct(element)), "url" => url_from_element(element), "type" => type_from_element(element)})
     |> Repo.insert()
  end

  defp maps_with_string_keys(element) do
    Map.new(element, fn {k, v} -> {Atom.to_string(k), v} end)
  end

  defp type_from_element(element) do
    element.__struct__
    |> Module.split()
    |> Enum.take(-1)
    |> List.first()
    |> String.downcase()
    |> String.to_existing_atom()
  end

  defp element_is_text(element) do
    type_from_element(element) != :text
  end

  defp url_from_element(element) do
    if element_is_text(element) do
      element.force_url
    else
      nil
    end
  end
end
