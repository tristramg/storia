defmodule Storia.Stories.Story do
  @moduledoc """
  Represents a story
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Logger
  alias Storia.Stories.{Story, Tag, TitleSlug}
  alias Storia.Accounts.Account
  alias Storia.Files.File

  schema "stories" do
    field :title, :string
    field :description, :string
    field :elements, {:array, Ecto.UUID}
    field :elements_entities, {:array, Story}, virtual: true
    field :published_at, :utc_datetime
    belongs_to :account, Account
    has_many :tags, Tag
    belongs_to :media, File

    field :slug, TitleSlug.Type

    timestamps()
  end

  @doc false
  def changeset(%Story{} = story, attrs) do
    story
    |> cast(attrs, [:title, :elements, :description, :account_id, :media_id, :published_at])
    |> cast_assoc(:tags)
    |> validate_required([:title, :elements, :account_id])
    |> TitleSlug.maybe_generate_slug()
    |> TitleSlug.unique_constraint()
  end
end
