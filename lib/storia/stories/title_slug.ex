defmodule Storia.Stories.TitleSlug do
  @moduledoc """
  Slug system for Story title
  """
  alias Storia.Stories.Story
  use EctoAutoslugField.Slug, from: :title, to: :slug
  import Ecto.Query
  alias Storia.Repo

  def build_slug(sources, changeset) do
    slug = super(sources, changeset)
    build_unique_slug(slug, changeset)
  end

  defp build_unique_slug(slug, changeset) do
    query = from s in Story,
      where: s.slug == ^slug

    case Repo.one(query) do
      nil -> slug
      _story ->
        slug
        |> Storia.Slug.increment_slug()
        |> build_unique_slug(changeset)
    end
  end
end
