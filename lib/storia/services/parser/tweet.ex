defmodule Storia.Services.Parser.Tweet do
  @moduledoc """
  Represents a Tweet element.

  Twitter Parser that connects to the Twitter API to get metadata on a tweet
  """
  import Logger
  alias Storia.Services.Parser.Tweet
  alias Storia.Services.Parser

  #@enforce_keys [:id, :user, :created_at, :text, :force_url]
  defstruct [:id, :user, :text, :created_at, :images, :videos, :force_url, :hashtags, :mentions]
  @type t :: %Tweet{id: String.t, user: map(), text: String.t, created_at: %DateTime{}, images: list(), videos: list(), force_url: String.t, hashtags: list(), mentions: list()}

  @doc """
  Maps a list of Twitter URLs to their corresponding elements

  Both `https://twitter.com/<username>/status/<tweet_id>` and `https://twitter.com/statuses/<tweet_id>` forms are supported.

  ## Examples

      iex> parse_twitter_url_list(["https://twitter.com/valentinsocha/status/957685409160597507"])
      %Tweet{}

  """
  @spec parse_twitter_url_list(list(String.t)) :: list(Tweet.t)
  def parse_twitter_url_list(urls) do
    Logger.debug("Parsing url list and return Tweet elements")
    urls
    |> Enum.map(fn url -> Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_|i\/web\/]+\/status\/([0-9]+)/iu, url, [capture: :all_but_first]) end)
    |> Enum.chunk(80, 80, [])
    |> parse_twitter_codes_list()
  end

  @doc """
  Maps a list of Twitter URLs to their corresponding elements, while respecting order
  """
  @spec parse_list_with_index(list(), list({%{required(atom()) => String.t, required(atom()) => atom()}, integer()})) :: list()
  def parse_list_with_index(return_list, data_list) do
    Logger.debug("Parsing element list for Twitter elements")

    # Find all elements
    elements = data_list
      |> Enum.map(fn {element, index} -> element.url end)
      |> parse_twitter_url_list()
    Logger.debug("Fetched all Twitter elements")

    # Map elements found to correct positions
    Parser.map_elements_to_positions(elements, data_list, return_list)
  end

  @spec parse_list_with_index(list(), []) :: list()
  def parse_list_with_index(return_list, []) do
    Logger.debug("Parsing element list for Twitter elements, but there's none to get")
    return_list
  end

  @doc """
  Processes a list of Twitter codes and fetch tweets
  """
  @spec parse_twitter_codes_list(List.t) :: List.t
  defp parse_twitter_codes_list([codes | tail]) do
    Logger.debug("Fetching Tweet elements")
    try do
      tweets = codes |> Enum.join(",")
      Logger.debug(inspect tweets)
      tweets = Enum.map(ExTwitter.lookup_status(tweets, [tweet_mode: "extended"]), fn tweet -> process_tweet(tweet) end)
      tweets ++ parse_twitter_codes_list(tail)
    rescue
      e in ExTwitter.Error ->
        Logger.error(inspect e)
      e in Poison.SyntaxError ->
        Logger.error(inspect e)
    end
  end

  @doc false
  @spec parse_twitter_codes_list(List.t) :: List.t
  defp parse_twitter_codes_list([]) do
    []
  end

  @doc """
  Parses a single Twitter URL to a Tweet element
  """
  @spec parse_single_url(String.t) :: Storia.Servces.Parser.Tweet.t
  def parse_single_url(tweet_url) do
    [code] = Regex.run(~r/http(?:s)?:\/\/(?:www.)?twitter\.com\/[a-zA-Z0-9_|i\/web\/]+\/status\/([0-9]+)/iu, tweet_url, [capture: :all_but_first])
    tweet = ExTwitter.lookup_status(code, [tweet_mode: "extended"])
    process_tweet(tweet)
  end

  @doc """
  Find the best video between all offered by comparing their bitrate
  """
  @spec find_best_video(list(map()), map()) :: map()
  defp find_best_video([source|tail], video \\ %{bitrate: 0}) do
    if Map.has_key?(source, :bitrate) && source.bitrate > video.bitrate do
      find_best_video(tail, source)
    else
      find_best_video(tail, video)
    end
  end

  @spec find_best_video([], map()) :: map()
  defp find_best_video([], video) do
    video
  end

  @doc """
  Creates a `Storia.Services.Parser.Tweet` struct from `ExTwitter.Model.Tweet` information
  """
  @spec process_tweet(ExTwitter.Model.Tweet.t) :: Storia.Services.Parser.Tweet.t
  def process_tweet(%ExTwitter.Model.Tweet{} = tweet) do
    imgs = if tweet.extended_entities[:media] do
      Enum.filter(Enum.map(tweet.extended_entities.media, fn img ->
        if img.type == "photo" do
          img.media_url_https
        end
      end), fn img -> img != nil end)
    else
      []
    end
    videos = if tweet.extended_entities[:media] do
      Enum.filter(Enum.map(tweet.extended_entities.media, fn video ->
        if video.type == "video" do
          %{"preview" => video.media_url_https, "video" => find_best_video(video.video_info.variants).url}
        end
      end), fn video -> video != nil end)
    else
      []
    end
    mentions = if tweet.entities[:user_mentions] do tweet.entities.user_mentions else [] end
    hashtags = if tweet.entities[:hashtags] do tweet.entities.hashtags else [] end
    %Tweet{
      id: tweet.id_str,
      user: tweet.user,
      text: cleanText(tweet),
      created_at: tweet.created_at,
      images: imgs,
      videos: videos,
      force_url: "https://twitter.com/#{tweet.user.screen_name}/status/#{tweet.id_str}",
      hashtags: hashtags,
      mentions: mentions,
    }
  end

  @doc """
  Unused Get Twitter Rate API Status

  TODO : Use me or remove me
  """
  defp get_twitter_rate_limit() do
    ExTwitter.rate_limit_status()
  end

  @doc """
  Cleans text from a tweet by replacing t.co links with originals and stripping media URLs
  """
  @spec cleanText(ExTwitter.Model.Tweet.t) :: String.t
  defp cleanText(%ExTwitter.Model.Tweet{} = tweet) do
    text = processLinks(tweet.full_text, tweet)
    if tweet.entities[:media] do
      stripImageURLs(text, tweet.entities.media)
    else
      text
    end
  end

  @doc """
  Replaces multiple t.co links in the tweet text with their original link
  """
  @spec processLinks(String.t, ExTwitter.Model.Tweet.t) :: String.t
  defp processLinks(text, tweet) do
    proccessLink(text, tweet.entities.urls)
  end

  @doc """
  Replaces a t.co link in the tweet text with their original link
  """
  @spec proccessLink(String.t, list(map())) :: String.t
  defp proccessLink(text, [head | tail]) do
    text
    |> String.replace(head.url, head.expanded_url)
    |> proccessLink(tail)
  end

  @spec proccessLink(String.t, []) :: String.t
  defp proccessLink(text, []) do
    text
  end

  @doc """
  Strips Image URLs from the tweet text
  """
  @spec stripImageURLs(String.t, list(map())) :: String.t
  defp stripImageURLs(text, [head | tail]) do
    text
    |> String.replace(head.url, "")
    |> stripImageURLs(tail)
  end

  @spec stripImageURLs(String.t, list()) :: String.t
  defp stripImageURLs(text, []) do
    text
  end
end
