defmodule Storia.Services.Parser.Facebook do
  @moduledoc """
  Facebook Parser that just tries to parse basic ogp tags
  """
  alias Storia.Services.Parser.Facebook
  import Meeseeks.XPath

  defstruct [:title, :description, :url, :author, :image, :force_url]

  def parse_facebook_url(url) do
    # Facebook forces us to use browser user-agents
    {:ok, response} = HTTPoison.get(url, ["User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0"])
    document = Meeseeks.parse(response.body)
    facebook = %Facebook{
      title: Meeseeks.attr(Meeseeks.one(document, xpath("//meta[@property='og:title']")), "content"),
      description: Meeseeks.attr(Meeseeks.one(document, xpath("//meta[@property='og:description']")), "content"),
      author: Meeseeks.attr(Meeseeks.one(document, xpath("//meta[@name='author']")), "content"),
      url: url,
      image: Meeseeks.attr(Meeseeks.one(document, xpath("//meta[@property='og:image']")), "content"),
      force_url: url,
    }
    {:ok, facebook}
  end
end
