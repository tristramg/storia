defmodule Storia.Services.Import.Storify do
  @moduledoc """
  Imports data from Storify stories.
  """
  @stories_per_page 50
  alias Storia.Services.Import
  import Logger
  import Meeseeks.XPath
  alias Storia.Services.Parser.Element
  alias Storia.Services.Parser

  @doc """
  Creates a story by importing a Storify story, using Storify's private API (while it lasts)

  The URL can either be the URL to the Story or the URL to it's XML representation

  ## Examples

      iex> import_from_url("https://storify.com/clarabdx/madeleineproject-saison-5")
      %{title: "#Madeleineproject SAISON 5", description: "Production Lumento avec la Radio Nova", elements: []}
  """
  @spec import_from_url(String.t) :: map()
  def import_from_url(url) do
    if String.match?(url, ~r/(https:\/\/)?storify\.com\/[a-z0-9\-_]+(\/[a-z0-9\-_]+)?/iu) do
      if String.match?(url, ~r/(\.xml$)/iu) do
        case HTTPoison.get url do
          {:ok, response} ->
            import_from_xml(response.body)
          {:error, %HTTPoison.Error{id: nil, reason: :timeout}} ->
            import_from_url(url)
        end
      else
        import_from_url(url <> ".xml")
      end
    else
      {:error, :not_storify}
    end
  end

  @doc """
  Imports all stories made by a Storify account

  ## Examples

      iex> import_from_account_name("clarabdx")
      [%{title: "#Madeleineproject SAISON 5", description: "Production Lumento avec la Radio Nova", elements: []}]
  """
  @spec import_from_account_name(pid(), String.t) :: list(map())
  def import_from_account_name(pid, account_name) do
    stories_url = list_from_account_name(pid, account_name)
    import_from_urls(pid, stories_url)
  end

  @doc """
  Import from URLs
  """
  @spec import_from_urls(pid(), list(String.t)) :: list(map())
  defp import_from_urls(pid, [head|tail]) do
    Logger.debug("Importing urls")
    [import_from_url(head.url)] ++ import_from_urls(pid, tail)
  end

  defp import_from_urls(pid, []) do
    []
  end

  @doc """
  Lists stories made by a Storify account
  """
  @spec list_from_account_name(pid(), String.t) :: list(map())
  def list_from_account_name(pid, account_name) do
    fetch_stories(pid, account_name)
  end

  @doc """
  Use Storify's private API to fetch stories
  """
  @spec fetch_stories(pid(), String.t, integer()) :: list(map())
  defp fetch_stories(parent, account_name, page \\ 1) do
    Logger.debug("Finding page " <> Integer.to_string(page))
    {:ok, response} = HTTPoison.get "https://private-api.storify.com/v1/stories/#{account_name}?page=#{page}&per_page=#{@stories_per_page}&sort=date.created&filter=published"
    case find_stories(response.body) do
      {:ok, stories} ->
        send(parent, {:fetched_page, stories, page})
        fetch_stories(parent, account_name, page + 1)
      {:end, stories} ->
        send(parent, {:finished, stories, page})
        stories
    end
  end

  @doc """
  Find stories inside each page of the private API
  """
  @spec find_stories(String.t) :: {atom(), map()}
  defp find_stories(body) do
    content = Poison.decode!(body, keys: :atoms)
    stories = content.content.stories
              |> Enum.map(fn story -> %{
                                        url: story.permalink,
                                        title: story.title,
                                        import: true,
                                        thumbnail: story.thumbnail,
                                        description: story.description,
                                        status: 0,
                                      } end)
    Logger.debug(inspect stories)
    if ((length stories) < content.content.per_page) do
      {:end, stories}
    else
      {:ok, stories}
    end
  end

  @doc """
  Import elements from XML data
  """
  @spec import_from_xml(String.t) :: map()
  defp import_from_xml(body) do
    document = Meeseeks.parse(body, :xml)
    elements = Meeseeks.all(document, xpath("/story/elements/item"))
    %{
      title: Meeseeks.text(Meeseeks.one(document, xpath("/story/title"))),
      elements: process_elements(elements),
      description: Meeseeks.text(Meeseeks.one(document, xpath("/story/description"))),
    }
  end

  @doc """
  Process multiple elements from Storify
  """
  @spec process_elements(list()) :: list(%Element{})
  defp process_elements(elements) do
    elements
    |> Enum.map(fn element ->
      element
      |> Meeseeks.one(xpath("//permalink"))
      |> Meeseeks.text()
    end)
    |> Parser.process_multiple_urls()
  end

  @spec process_elements([]) :: []
  defp process_elements([]) do
    []
  end
end
