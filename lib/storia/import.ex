defmodule Storia.Import do
  @moduledoc """
  Represents an import to process
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Storia.Import
  alias Storia.Accounts.Account


  schema "imports" do
    field :tentatives, :integer, default: 0
    field :url, :string
    belongs_to :account, Account

    timestamps()
  end

  @doc false
  def changeset(%Import{} = import, attrs) do
    import
    |> cast(attrs, [:url, :tentatives, :account_id])
    |> validate_required([:url, :account_id])
  end
end
