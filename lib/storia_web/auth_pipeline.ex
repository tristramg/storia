defmodule StoriaWeb.AuthPipeline do
  @moduledoc """
  Makes sure the user's logged in
  """

  use Guardian.Plug.Pipeline, otp_app: :storia,
                              module: StoriaWeb.Guardian,
                              error_handler: StoriaWeb.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource, ensure: true

end
