defmodule StoriaWeb.ImportStorifyChannel do
  @moduledoc """
  Channel to import Storify Stories
  """
  use Phoenix.Channel
  alias Storia.Services.Import.Storify
  alias Storia.Stories
  alias Storia.Stories.Story
  alias Storia.Import
  alias Storia.Imports
  alias Storia.Repo
  alias Storia.Accounts
  alias StoriaWeb.ImportStorifyChannel
  import Logger

  def join("import:storify:" <> username, _message, socket) do
    {:ok, socket}
  end

  def handle_in("profile", %{"body" => body}, socket) do
    Logger.debug("Get profile")
    Task.start(Storify, :list_from_account_name, [self(), body["account_name"]])
    {:ok, stories, page} = wait_for_stories_list(socket)
    broadcast! socket, "return_profile", %{stories: stories, page_total: page}
    {:reply, :ok, socket}
  end

  defp wait_for_stories_list(socket, previous_stories \\ []) do
    receive do
      {:fetched_page, stories, page} ->
        broadcast! socket, "fetched_page", %{stories: stories,page: page}
        wait_for_stories_list(socket, previous_stories ++ stories)
      {:finished, stories, page} ->
        {:ok, previous_stories ++ stories, page}
    end
  end

  def handle_in("import", %{"body" => body}, socket) do
    stories_to_import = body["stories"]
    |> save_stories(socket)
    |> process_stories(socket)
  end

  defp process_stories([head | tail], socket) do
    user_id = socket.assigns.user.id
    Task.start(ImportStorifyChannel, :process_from_url, [self(), head.url, user_id])
    receive do
      {:ok, story, url} ->
        remove_import(url)
        broadcast! socket, "processed_story", %{story: %{slug: story.slug, url: url, status: :ok}}
      {:error, url} ->
        remove_import(url)
        broadcast! socket, "processed_story", %{story: %{url: url, status: :failure}}
    end
    process_stories(tail, socket)
  end

  defp process_stories([], socket) do
    broadcast! socket, "finished", %{}
    {:reply, :ok, socket}
  end

  defp remove_import(url) do
    url
    |> Imports.get_import_by_url()
    |> Imports.delete_import()
  end

  def process_from_url(parent, url, user_id) do
    params = Storify.import_from_url(url)
    params = Map.put(params, :elements, process_elements(params.elements))
    params = Map.put(params, :account_id, user_id)
    case Stories.create_story(params) do
      {:ok, %Story{} = story} ->
        send(parent, {:ok, story, url})
      {:error} ->
        send(parent, {:error, url})
    end
  end

  defp process_elements(elements) do
    Enum.map(elements, fn element ->
      {:ok, element} = Stories.create_element(element)
      element.id
    end)
  end

  defp save_stories([head | tail], socket) do
    [save_import(head, socket.assigns.user.id)] ++ save_stories(tail, socket)
  end

  defp save_stories([], socket) do
    broadcast! socket, "saved_stories", %{}
    []
  end

  defp save_import(story, user_id) do
    attrs = %{"url" => story["url"], "account_id" => user_id}
    {:ok, import} = Imports.create_import(attrs)
    import
  end
end
