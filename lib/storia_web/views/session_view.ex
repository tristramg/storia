defmodule StoriaWeb.SessionView do
  use StoriaWeb, :view

  def render("token.json", %{token: token, user: user}) do
    %{token: token, user: render_one(user, StoriaWeb.AccountView, "account.json")}
  end
end
