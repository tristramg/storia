defmodule StoriaWeb.StoryView do
  use StoriaWeb, :view
  alias StoriaWeb.StoryView
  alias StoriaWeb.AccountView
  alias StoriaWeb.FileView
  alias StoriaWeb.ElementView
  import Logger
  alias StoriaWeb.{TweetView, OpenGraphView, BasicView, YoutubeView}

  def render("index.json", %{stories: stories, page: page}) do
    %{
      stories: render_many(stories, StoryView, "story_simple.json"),
      page: page,
     }
  end

  def render("show.json", %{story: story}) do
    %{story: render_one(story, StoryView, "story.json")}
  end

  def render("story_simple.json", %{story: story}) do
    %{id: story.id,
      title: HtmlSanitizeEx.strip_tags(story.title),
      created_at: story.inserted_at,
      updated_at: story.updated_at,
      slug: story.slug,
      description: HtmlSanitizeEx.strip_tags(story.description),
      media: render_one(story.media, FileView, "file.json"),
      nb_elements: length(story.elements),
      user: render_one(story.account, AccountView, "account.json"),
      published_at: story.published_at,
    }
  end

  def render("story.json", %{story: story}) do
    %{id: story.id,
      title: HtmlSanitizeEx.strip_tags(story.title),
      created_at: story.inserted_at,
      updated_at: story.updated_at,
      slug: story.slug,
      description: HtmlSanitizeEx.strip_tags(story.description),
      media: render_one(story.media, FileView, "file.json"),
      elements: render_one(story.elements, StoryView, "elements.json"),
      user: render_one(story.account, AccountView, "account.json"),
      published_at: story.published_at,
    }
  end

  def render("elements.json", %{story: elements_paged}) do
    %{
      entries: render_many(elements_paged.entries, ElementView, "element.json"),
      total_pages: elements_paged.total_pages,
      total_entries: elements_paged.total_entries,
      page_number: elements_paged.page_number,
      page_size: elements_paged.page_size,
    }
  end

  def render("element_simple.json", %{story: element}) do
    element
  end
end
