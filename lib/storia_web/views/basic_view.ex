defmodule StoriaWeb.BasicView do
  use StoriaWeb, :view

  def renderBasic(%{} = metadata) do
    render("basic.html", %{metadata: metadata})
  end

  def render("basic.json", %{} = metadata) do
    metadata = metadata.basic
    %{
      type: "basic",
      title: metadata.title,
      description: metadata.description,
      url: metadata.url,
      id: metadata.url,
      author: metadata.author,
      domain: urlToDomain(metadata.url),
    }
  end

  def urlToDomain(url) do
    fuzzyurl = Fuzzyurl.from_string(url)
    fuzzyurl.hostname
    |> stripwww
  end

  def stripwww("www." <> domain) do domain end

  def stripwww(domain) do domain end
end