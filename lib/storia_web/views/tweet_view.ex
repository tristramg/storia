defmodule StoriaWeb.TweetView do
  alias Storia.Services.Parser.Tweet
  import Logger
  use StoriaWeb, :view

  def render("tweet.json", %{tweet: %Tweet{} = tweet}) do
    %{
      type: "tweet",
      text: toLinks(tweet),
      user: %{username: tweet.user.name, screen_name: tweet.user.screen_name, profile: tweet.user.profile_image_url},
      id: tweet.id,
      images: tweet.images,
      videos: tweet.videos,
      created_at: to_unix(tweet.created_at),
    }
  end

  def render("tweet.json", %{tweet: %{created_at: created_at} = tweet}) do
    render("tweet.json", %{tweet: struct(Tweet, tweet)})
  end

  def render("tweet.json", %{tweet: %{"created_at" => created_at} = tweet}) do
    tweet = keys_to_atoms(tweet)
    render("tweet.json", %{tweet: tweet})
  end

  def keys_to_atoms(string_key_map) when is_map(string_key_map) do
    for {key, val} <- string_key_map, into: %{}, do: {String.to_atom(key), keys_to_atoms(val)}
  end
  def keys_to_atoms(value), do: value

  def toLinks(tweet) do
    urls = Regex.scan(~r/(http|f  tp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/iu, tweet.text, [capture: :first])
    text = urls
      |> processURLs()
      |> replaceURLs(keys_to_atoms(tweet.text))
      |> processHashtags(keys_to_atoms(tweet.hashtags))
      |> processUsers(keys_to_atoms(tweet.mentions))
  end

  defp processURLs(urls) do
    Enum.map(urls, fn url -> %{url: url, html: processURL(url)} end)
  end

  def processURL([url]) do
    "<a class=\"tweet-card-link\" href=\"" <> url <> "\">" <> url <> "</a>"
  end

  defp replaceURLs([head | tail], body) do
    body = replaceURLs(tail, body)
    body = replaceURL(head, body)
    body
  end

  defp replaceURLs([], body) do
    body
  end

  defp replaceURL(url_map, body) do
    String.replace(body, hd(url_map.url), url_map.html)
  end

  defp processHashtags(text, [head | tail]) do
    Logger.debug(inspect head)
    head = keys_to_atoms(head)
    text
    |> String.replace("##{head.text}", "<a class=\"tweet-card-link\" href=\"https://twitter.com/hashtag/#{head.text}\">##{head.text}</a>")
    |> processHashtags(tail)
  end

  defp processHashtags(text, []) do
    text
  end

  defp processUsers(text, [head | tail]) do
    head = keys_to_atoms(head)
    text
    |> String.replace("@#{head.screen_name}", "<a class=\"tweet-card-link\" href=\"https://twitter.com/#{head.screen_name}\">@#{head.screen_name}</a>")
    |> processUsers(tail)
  end

  defp processUsers(text, []) do
    text
  end

  def to_date(string_date) do
    {:ok, datetime} = Timex.parse(string_date, "{WDshort} {Mshort} {D} {h24}:{m}:{s} {Z} {YYYY}")
    {:ok, datetime_formatted} = Timex.format(datetime, "{D} {Mshort} {YYYY} {h24}:{m}:{s}")
    datetime_formatted
  end

  def to_unix(string_date) do
    {:ok, datetime} = Timex.parse(string_date, "{WDshort} {Mshort} {D} {h24}:{m}:{s} {Z} {YYYY}")
    DateTime.to_unix(datetime)
  end

  def to_struct(kind, attrs) do
    struct = struct(kind)
    Enum.reduce Map.to_list(struct), struct, fn {k, _}, acc ->
      case Map.fetch(attrs, Atom.to_string(k)) do
        {:ok, v} -> %{acc | k => v}
        :error -> acc
      end
    end
  end
end
