defmodule StoriaWeb.Router do
  use StoriaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_auth do
    plug :accepts, ["json"]
    plug StoriaWeb.AuthPipeline
  end

  scope "/api", StoriaWeb do
    pipe_through :api

    post "/users", AccountController, :create
    resources "/users", AccountController, only: [:show, :options], param: "slug"
    post "/login", SessionController, :sign_in
    resources "/stories", StoryController, only: [:show, :index], param: "slug"
    get "/stories/:slug/elements/:page", StoryController, :fetch_elements_for_story
    get "/users/:slug/stories", StoryController, :index_for_user
    get "/search/:search", StoryController, :search
  end

   scope "/api", StoriaWeb do
     pipe_through :api_auth

     post "/logout", SessionController, :sign_out
     get "/user", AccountController, :show_current_account
     resources "/stories", StoryController, except: [:new, :edit, :show, :index], param: "slug"
     get "/stories/:slug/all", StoryController, :show_all
     get "/stories/:slug/preview", StoryController, :show_preview
     resources "/elements", ElementController, only: [:create, :show]
     post "/import/storify/url", StoryController, :import
     resources "/tags", TagsController, except: [:new, :edit]
     resources "/users", AccountController, except: [:index, :show, :options, :create, :new, :edit], param: "slug"
  end

  scope "/api/doc" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :storia, swagger_file: "swagger.json"
  end

  scope "/files", StoriaWeb do
    pipe_through :api_auth
    options "/",  			UploadController, :options
    options "/:file",  		UploadController, :options
    match :head, "/:file",  UploadController, :head
    post "/",  				UploadController, :post
    patch "/:file",  		UploadController, :patch
    delete "/:file",  		UploadController, :delete
  end

  scope "/files", StoriaWeb do
    pipe_through :api
    get "/:file", FileController, :show
  end

  scope "/", StoriaWeb do
    pipe_through :browser # Use the default browser stack

    get "/*path", PageController, :index
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Storia"
      }
    }
  end
end
