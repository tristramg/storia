defmodule StoriaWeb.SessionController do
  use StoriaWeb, :controller
  use PhoenixSwagger

  alias Storia.Accounts.Account
  alias Storia.Accounts

  swagger_path :sign_in do
    summary "Sign-in"
    description "Sign-in with username and password"
    parameters do
      username :body, :string, "The username of the account", required: true
      password :body, :string, "The password of account", required: true
    end
    response 200, "Ok", (Schema.new do
      properties do
        token :string, "A JWT token", example: "eyJhbGciOiJIUz[...]"
        user Schema.ref(:Account), "The logged-in account"
      end
    end)
    response 401, "Unauthorized", Schema.ref(:AuthError)
  end
  def sign_in(conn, %{"username" => username, "password" => password}) do
    with %Account{} = user <- Accounts.find_by_username(username) do
      # Attempt to authenticate the user
      with {:ok, token, _claims} <- Accounts.authenticate(%{username: user, password: password}) do
        # Render the token
        render conn, "token.json", %{token: token, user: user}
      end
      send_resp(conn, 401, Poison.encode!(%{"error_msg" => "Bad login", "display_error" => "session.error.bad_login"}))
    end
    send_resp(conn, 401, Poison.encode!(%{"error_msg" => "No such user", "display_error" => "session.error.bad_login"}))
  end

  swagger_path :sign_out do
    summary "Sign-out"
    description "Sign-out"
    response 204, "No Content"
  end
  def sign_out(conn, _params) do
    conn
    |> StoriaWeb.Guardian.Plug.sign_out()
    |> send_resp(204, "")
  end

  def show(conn, _params) do
    user = StoriaWeb.Guardian.Plug.current_resource(conn)

    send_resp(conn, 200, Poison.encode!(%{user: user}))
  end

  def swagger_definitions do
  %{
    AuthError: swagger_schema do
      properties do
        error_msg :string, "A string identifier for the error"
        display_error :string, "A translation key to display for the error"
      end
      example %{
        "error_msg" => "Bad login",
        "display_error" => "session.error.bad_login"
      }
    end
  }
  end
end
