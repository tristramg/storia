defmodule StoriaWeb.ImportController do
  use StoriaWeb, :controller

  import Logger
  alias Storia.Stories
  alias Storia.Stories.Story
  alias Storia.Services.Import

  action_fallback StoriaWeb.FallbackController

  def index_for_user(conn, %{"userid" => userid}) do
    stories = Stories.list_stories(userid)
    render(conn, "index.json", stories: stories)
  end

  def import(conn, %{"url" => url}) do
    story = Import.Storify.import_from_url(url)
    params = Map.put(story, "account_id", Guardian.Plug.current_resource(conn).id)
    with {:ok, %Story{} = story} <- Stories.create_story(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", story_path(conn, :show, story))
      |> render("show.json", story: story)
    end
  end
end
