defmodule StoriaWeb.StoryController do
  use StoriaWeb, :controller
  use PhoenixSwagger

  import Logger
  alias Storia.Stories
  alias Storia.Stories.Story
  alias Storia.Services.Import.Storify
  alias Storia.Files.File
  alias Storia.Files
  alias Storia.Repo
  alias Storia.Services.Parser

  action_fallback StoriaWeb.FallbackController

  def index(conn, _params) do
    page = Stories.list_stories(_params)
    render_paginated(conn, page)
  end

  def index_for_user(conn, %{"slug" => slug} = _params) do
    page = Stories.list_stories_for_account_slug(slug, _params)
    render_paginated(conn, page)
  end

  def search(conn, %{"search" => search} = _params) do
    page = Stories.search_stories(search, _params)
    render_paginated(conn, page)
  end

  defp render_paginated(conn, page) do
    conn
    |> Scrivener.Headers.paginate(page)
    |> render("index.json", stories: page.entries, page: %{page_number: page.page_number, page_size: page.page_size, total_pages: page.total_pages, total_entries: page.total_entries})
  end

  swagger_path :create do
    summary "Create story"
    description "Creates a new story from params"
    parameters do
      title :body, :string, "An title for the new story", required: true
      elements :body, :array, "An array of elements UUIDs for the new story", required: true
      description :body, :string, "A description for the new story", required: true
    end
    response 201, "Ok", Schema.ref(:Story)
    response 422, "Unprocessable Entity", Schema.ref(:Error)
  end
  def create(conn, %{"story" => story_params}) do
    params = story_params
      |> Map.put("account_id", Guardian.Plug.current_resource(conn).id)
    if story_params["media"] do
      file_id = Files.get_file_by_uuid!(story_params["media"]).id
      params = Map.put(params, "media_id", file_id)
    end
    Logger.debug(inspect params)
    with {:ok, %Story{} = story} <- Stories.create_story(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", story_path(conn, :show, story))
      |> render("show.json", story: story)
    end
  end

  @doc """
  Finds a story and load it's first elements

  ## Examples

      iex> process_story("my-story")
      {:commit, %Story{}}

      iex> process_story("inexistant")
      {:ignore, nil}
  """
  @spec process_story(String.t) :: {atom(), Story.t}
  defp process_story(slug, load_all \\ true) do
    case Stories.get_story_by_slug(slug, 0, 20, load_all) do
      %Story{} = story ->
        Logger.debug("Fetched story")
        {:commit, story}
      nil ->
        {:ignore, nil}
    end
  end

  @doc """
  Fetch elements for story.

  Used to load more elements for a story
  """
  def fetch_elements_for_story(conn, %{"slug" => slug, "page" => page}) do
    {page, _} = Integer.parse(page)
    with %Story{} = story <- Stories.get_story_by_slug(slug, page) do
      render(conn, "elements.json", story: story.elements)
    end
  end

  @doc """
  Load a story from the cache or fetch it
  """
  @spec cache_story(String.t) :: {atom(), any()}
  defp cache_story(slug) do
    cache = Cachex.get(:my_cache, slug, fallback: fn slug ->
      {status, story} = process_story(slug)
      case status do
        :commit ->
          {status, story}
        _ ->
          {:ignore, nil}
      end
    end)
    case cache do
      {success, result} when success in [:ok, :loaded] and result != nil ->
        {:ok, result}
      {error, nil} when error in [:loaded, :error] ->
        {:error, :not_found}
    end
  end

  swagger_path :show do
    summary "Show a story"
    description "Shows a story.
    Stories (and their 20 first elements) are cached so that they're not fetched for database each time"
    parameters do
      slug :path, :string, "The story's slug", required: true
    end
    response 200, "Ok", Schema.ref(:Story)
    response 404, "Not found", Schema.ref(:Error)
  end
  @doc """
  Shows a story
  """
  def show(conn, %{"slug" => slug}) do
    with {:ok, %Story{} = story} <- cache_story(slug) do
      if story.published_at != nil && DateTime.compare(story.published_at, DateTime.utc_now()) == :lt do # If the story has been published
        render(conn, "show.json", story: story)
      end
    end
  end

  @doc """
  Returns a story in preview mode
  """
  def show_preview(conn, %{"slug" => slug}) do
    with {:ok, %Story{} = story} <- cache_story(slug) do
      if story.account.id === Guardian.Plug.current_resource(conn).id do # If the preview is accessed by it's author
        render(conn, "show.json", story: story)
      end
    end
  end

  @doc """
  Returns a story with all it's elements.

  Used to get the edit pages
  """
  def show_all(conn, %{"slug" => slug}) do
    with {:commit, story} <- process_story(slug, true) do
      if story.account.id === Guardian.Plug.current_resource(conn).id do
        render(conn, "show.json", story: story)
      end
    end
  end

  # TODO : I shouldn't be here
  def import(conn, %{"url" => url}) do
    story = Storify.import_from_url(url)
    params = Map.put(story, "account_id", Guardian.Plug.current_resource(conn).id)
    with {:ok, %Story{} = story} <- Stories.create_story(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", story_path(conn, :show, story))
      |> render("show.json", story: story)
    end
  end

  # TODO : I shouldn't be here
  def render(conn, %{"url" => url}) do
    conn
    |> put_resp_header("content-type", "application/json")
    |> resp(:ok, Phoenix.View.render_to_string(StoriaWeb.StoryView, "element.json", %{element: Parser.process_single_url(url)}))
  end

#  defp show_by_id(conn, id) do
#    story = Stories.get_story!(id)
#    render(conn, "show.json", story: story)
#  end

  swagger_path :update do
    summary "Update story"
    description "Update a story's details"
    parameters do
      slug :path, :string, "The story's slug", required: true
      account :body, Schema.ref(:Story), "The story details to update"
    end
    response 200, "Ok", Schema.ref(:Story)
    response 404, "Not found", Schema.ref(:Error)
  end
  def update(conn, %{"slug" => slug, "story" => story_params}) do
    story = Stories.get_story_by_slug(slug)
    story_account_id = story.account.id

    if story_params["media"] do
      Logger.debug("We have a media !")
      file_id = Files.get_file_by_uuid!(story_params["media"]).id
      story_params = Map.put(story_params, "media_id", file_id)
      Logger.debug(inspect story_params)
    end

    case Guardian.Plug.current_resource(conn).id do
      story_account_id ->
        with {:ok, %Story{} = story} <- Stories.update_story(story, story_params) do
          Cachex.del(:my_cache, slug)
          with {:commit, story} <- process_story(slug, true) do
            render(conn, "show.json", story: story)
          end
        end
      _ ->
        send_resp(conn, 400, "This isn't your story !")
    end
  end

  swagger_path :delete do
    summary "Delete story"
    description "Delete a story"
    parameters do
      slug :path, :string, "The story's slug", required: true
    end
    response 204, "Ok", Schema.ref(:Story)
    response 401, "Unauthorized", Schema.ref(:Error)
    response 404, "Not found", Schema.ref(:Error)
  end
  def delete(conn, %{"slug" => slug}) do
    story = Stories.get_story_by_slug(slug)
    if Guardian.Plug.current_resource(conn).id != story.account_id do
      send_resp(conn, 400, "This isn't your story !")
    else
      with {:ok, %Story{}} <- Stories.delete_story(story) do
        Cachex.del(:my_cache, slug)
        send_resp(conn, :no_content, "")
      end
    end
  end

  def swagger_definitions do
    %{
      Story: swagger_schema do
        title "Story"
        description "Represents a story"
        properties do
          title :string, "The story's title", required: true
          slug :string, "The story's slugged title", required: true
          description :string, "The story's description", required: false
          account Schema.ref(:Account), "The story's author's account", required: true
          inserted_at :string, "When was the story initially created", format: "ISO-8601"
          elements (Schema.new do
            description "Represents paginated elements for this story. At first we have 20 elements with the story, we must call /stories/:slug/:page to get more"
            properties do
             entries Schema.ref(:Elements), "A subset of elements for this story"
             total_entries :integer, "The total number of elements for this story"
             total_pages :integer, "The total number of pages of elements for this story"
             page_number :integer, "The index of the current page"
             page_size :integer, "The number of elements per page"
           end
          end)
        end
        example %{
          title: "my awesome story",
          slug: "my-awesome-story",
          description: "A story of myths and dragons"
        }
      end,
      Error: swagger_schema do
        title "Errors"
        description "Error responses from the API"
        properties do
          error :string, "The message of the error raised", required: true
        end
      end
    }
  end
end
