use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :storia, StoriaWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :storia, Storia.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: if(System.get_env("CI"), do: "postgres", else: "elixir"),
  password: if(System.get_env("CI"), do: "", else: "elixir"),
  database: "storia_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox

config :plug, :validate_header_keys_during_test, false
