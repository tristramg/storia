import Vue from 'vue';
import Router from 'vue-router';
import PageNotFound from '@/components/PageNotFound';
import Home from '@/components/Home';
import Stories from '@/components/StoryList';
import Search from '@/components/Search';
import Story from '@/components/story/Story';
import EditStory from '@/components/story/EditStory';
import NewStory from '@/components/story/NewStory';
import ImportStorifyProfile from '@/components/import/storify/ImportProfile';
import ImportStorifyURL from '@/components/import/storify/ImportURL';
import ImportStorify from '@/components/import/storify/ImportStorify';
import ImportTwitter from '@/components/import/twitter/ImportTwitter';
import ImportTwitterMoment from '@/components/import/twitter/ImportTwitterMoment';
import ImportTwitterTweets from '@/components/import/twitter/ImportTwitterTweets';
import Login from '@/components/Login';
import Register from '@/components/Register';
import Profile from '@/components/Profile';
import EditProfile from '@/components/EditProfile';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/stories',
      name: 'Stories',
      component: Stories,
    },
    {
      path: '/stories/:search_term',
      name: 'Search',
      component: Search,
      props: true,
    },
    {
      path: '/story/:slug',
      name: 'Story',
      component: Story,
      props: true,
    },
    {
      path: '/story/:slug/edit',
      name: 'EditStory',
      component: EditStory,
      props: true,
    },
    {
      path: '/new',
      name: 'NewStory',
      component: NewStory,
    },
    {
      path: '/import/storify',
      name: 'ImportStorify',
      component: ImportStorify,
    },
    {
      path: '/import/storify/url',
      name: 'ImportStorifyURL',
      component: ImportStorifyURL,
    },
    {
      path: '/import/storify/profile',
      name: 'ImportStorifyProfile',
      component: ImportStorifyProfile,
    },
    {
      path: '/import/twitter',
      name: 'ImportTwitter',
      component: ImportTwitter,
    },
    {
      path: '/import/twitter_moment',
      name: 'ImportTwitterMoment',
      component: ImportTwitterMoment,
    },
    {
      path: '/import/twitter_tweets',
      name: 'ImportTwitterTweets',
      component: ImportTwitterTweets,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
    },
    {
      path: '/profile/:slug',
      name: 'Profile',
      component: Profile,
      props: true,
    },
    {
      path: '/profile/:slug/edit',
      name: 'EditProfile',
      component: EditProfile,
      props: true,
    },
    { path: '*',
      name: 'PageNotFound',
      component: PageNotFound,
      meta: { requiredAuth: false },
    },
  ],
});
