export default {
  storia: {
    app_name: 'Storia',
    desc: 'A free software Storify-clone, currently in beta',
  },
  home: {
    buttons: {
      view_stories: 'View storias',
      add_story: 'Add a storia',
      login: 'Login',
      register: 'Register',
    },
    columns: {
      whats: {
        title: 'What\'s this app?',
        paragraph: 'It\'s a Storify-like app to create and publish storias relying on content from external sources, like tweets',
        example: 'You can have a look to <a href="https://storia.tcit.fr/story/mon-histoire">this example</a>',
        sources: 'The sources for this app are available on our Gitlab repository : <a href="https://framagit.org/tcit/storia">https://framagit.org/tcit/storia</a>',
      },
      why_name: {
        title: 'Why the name?',
        paragraph: 'It\'s basically italian for "Story". I\'m a really shitty name picker.',
      },
      what_uses: {
        title: 'What did you use to make this?',
        paragraph: '<strike>PHP</strike>. The API server is running with Elixir, with the Phoenix Framework. The front-end uses VueJS and Bootstrap.',
      },
      what_stories: {
        title: 'What are storias?',
        paragraph: 'It\'s like a blog post, but centered on content which is coming from other sources, like tweets or videos. You just need to put some URLs and the content and / or metadata will be fetched automatically.',
        sources_list: {
          paragraph: 'The supported sources are the following:',
          elements: {
            twitter: 'Twitter tweets',
            youtube: 'YouTube videos',
            facebook: 'Facebook (basic)',
            opengraph: 'Any webpage with OpenGraph data',
            basic: 'Any webpage with basic <code>&lt;meta&gt;</code> elements',
          },
          more: 'Of course, more are coming.',
        },
        import: {
          title: 'Import',
          paragraph: 'You can import your storias from the following sources:',
          sources: {
            storify: 'Storify stories (you can import all stories from an user at once',
            twitter: 'Twitter Moments',
          },
        },
      },
      more_questions: {
        title: 'Why, when, who?',
        why: {
          title: 'Why?',
          paragraph: 'Storify <a href="https://storify.com/faq-eol">has announced</a> it would be taken offline on May 16, 2018. And it was quite an useful tool. For some people.',
        },
        when: {
          title: 'When?',
          paragraph: 'This app will be available when it\'s ready ;) However, it\'s already working and available. So I guess it\'s ready.',
        },
        who: {
          title: 'Who?',
          paragraph: 'This app is made by a <a href="https://framasoft.org/">Framasoft</a> employee.',
        },
      },
    },
  },
  navbar: {
    new: 'New',
    browse: 'Browse',
    import_from_storify: 'Import from Storify',
    import_from_twitter: 'Import from Twitter',
    search: 'Search',
    profile: 'Profile',
    sign_out: 'Sign out',
  },
  footer: {
    made_by: 'Made with <3 by Thomas Citharel',
  },
  story: {
    last_updated_on: 'Last updated {time}',
    view_story: 'View story',
    create_story: 'Create story',
    no_stories: 'There\'s no stories',
    form: {
      empty_body: 'Body of your story can\'t be empty',
      invalid: 'Form is invalid',
    },
  },
  profile: {
    hello: 'Hi {user}',
    no_stories: 'You don\'t have any storias yet, do you want to create one?',
    no_stories_anon: '{user} hasn\'t published any storias yet',
    form: {
      display_name: {
        label: 'Displayed name',
        desc: 'The name which will be displayed on your storias and on your profile.',
      },
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      avatar: 'Avatar',
      update_btn: 'Update my profile',
      cancel_btn: 'Cancel',
    },
    stats: {
      stories: '{stories} storias published',
    },
  },
  session: {
    error: {
      bad_login: 'Your username or password is incorrect',
    },
    success: {
      login: 'Welcome on Storia, {username}',
    },
    form: {
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      login_btn: 'Login',
      cancel_btn: 'Cancel',
    },
  },
  registration: {
    form: {
      email: {
        label: 'Email',
        desc: 'Your email will never be shared with anyone',
      },
      username: {
        label: 'Username',
      },
      password: {
        label: 'Password',
        desc: 'Please be strong',
      },
      register_btn: 'Register',
      cancel_btn: 'Cancel',
    },
    error: {
      email_already_used: 'This email is already in use',
      username_already_used: 'This username is already in use',
      password_too_short: 'Your password is too short, it must be at least 6 caracters',
    },
    success: {
      login: 'Your are now registered on Storia, welcome!',
    },
  },
};
