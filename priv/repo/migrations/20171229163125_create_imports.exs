defmodule Storia.Repo.Migrations.CreateImports do
  use Ecto.Migration

  def change do
    create table(:imports) do
      add :url, :string, null: false
      add :tentatives, :integer, [null: false, default: 0]
      add :account_id, references(:accounts)

      timestamps()
    end

  end
end
