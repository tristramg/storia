defmodule Storia.Repo.Migrations.SetDescriptionAsText do
  use Ecto.Migration

  def change do
    alter table(:stories) do
      modify :description, :text
    end

  end
end
