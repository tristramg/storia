defmodule Storia.Repo.Migrations.SetElementUrlUnique do
  use Ecto.Migration

  def change do
    create unique_index(:elements, [:url])
  end
end
