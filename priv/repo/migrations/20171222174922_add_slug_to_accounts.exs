defmodule Storia.Repo.Migrations.AddSlugToAccounts do
  use Ecto.Migration

  def change do
    alter table("accounts") do
      add :slug, :string, null: false
    end
  end
end
