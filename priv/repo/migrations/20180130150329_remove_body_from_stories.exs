defmodule Storia.Repo.Migrations.RemoveBodyFromStories do
  use Ecto.Migration

  def change do
    alter table(:stories) do
      remove :body
    end
  end
end
